
import java.io.File;
import java.io.IOException;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows
 */

public class main {
    public static void llenarJugador(jugador nuevo){
        String nom = JOptionPane.showInputDialog("Nombre del jugador");
        nuevo.setNombre(nom);
        String col = JOptionPane.showInputDialog("Color del auto");
        nuevo.setColor(col);
    }
    public static int menu(){
        int opcion = 0;
        do{
            opcion = Integer.parseInt(JOptionPane.showInputDialog(" ===== MENU DE SELECCION DE OPCIONES =====\n"+
                    "1. crear jugadores \n"+
                    "2. iniciar simulacion de juego \n"+
                    "3. finalizar \n"
                    ));
        }while(opcion <= 0 || opcion > 3);
        return opcion;
    }
    public static String podio(pista p) throws IOException{
        archivo a = new archivo();
        String datos = "";
        datos = datos + String.valueOf(p.getPuesto1() + " \n" 
                + p.getPuesto2() + " \n" 
                + p.getPuesto3() + " \n"
                + " Pista recorrida: "+ p.getIdp());
        a.guardar(p);
        return datos;
        
    }
    
    public static void main(String[] args) throws IOException{
        logica_juego perfil = new logica_juego();
        int opc,Opcpist;
        int contC=0;
        jugador aux;
        pista pst1,pst2,pst3,pst4;
         do{
             opc = menu();
             switch (opc){
                 case 1:
                     aux = new jugador();
                     llenarJugador(aux);
                     perfil.agregarJugador(aux);
                     contC++;
                     JOptionPane.showMessageDialog(null, "El carril asignado es "+contC);
                     break;
                 case 2:
                     
                        Opcpist = Integer.parseInt(JOptionPane.showInputDialog(" pistas disponibles \n"+
                             "1. medellin     Longitud 3500 m \n"+
                             "2. bogota       Longitud 3900 m \n"+
                             "3. barranquilla  Longitud 3400 m \n"+
                             "4. monteria     Longitud 4000 m"));
                             switch (Opcpist){
                                 case 1:
                                     pst1 = new pista();
                                     pst1.setIdp("Medellin");
                                     pst1.setDistancia(3500);
                                     perfil.iniciar(pst1);
                                     JOptionPane.showMessageDialog(null, podio(pst1));
                                     break;
                                 case 2:
                                     pst2 = new pista();
                                     pst2.setIdp("bogota");
                                     pst2.setDistancia(3900);
                                     perfil.iniciar(pst2);
                                     JOptionPane.showMessageDialog(null, podio(pst2));
                                     break;
                                 case 3:
                                     pst3 = new pista();
                                     pst3.setIdp("barranquilla");
                                     pst3.setDistancia(3400);
                                     perfil.iniciar(pst3);
                                     JOptionPane.showMessageDialog(null, podio(pst3));
                                     break;
                                 case 4:
                                     pst4 = new pista();
                                     pst4.setIdp("monteria");
                                     pst4.setDistancia(4000);
                                     perfil.iniciar(pst4);
                                     JOptionPane.showMessageDialog(null, podio(pst4));
                                     break;
                                    }
                 case 3:
                     perfil.limpiar();
                     break;
             }
         }while(opc != 3);
    }
}
