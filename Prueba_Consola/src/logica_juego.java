
import static java.lang.Math.random;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows
 */
public class logica_juego {
    private jugador cabeza;
    public void setCabeza(jugador cab){
        cabeza = cab;
    }
    public jugador getCabeza(){
        return cabeza;
    }
    public jugador ultimo(){
        jugador temp = cabeza;
        while(temp != null){
            if(temp.getSiguiente()== null){
                break;
            }else{
                temp = temp.getSiguiente();
            }
        }
        return temp;
    }
    
    public void eliminar(jugador nd){
        jugador anterior;
        if(nd == cabeza){
            cabeza = cabeza.getSiguiente();
        }else{
            anterior = cabeza;
            while(anterior.getSiguiente() != nd){
                anterior = anterior.getSiguiente();
            }
            anterior.setSiguiente(nd.getSiguiente());
        }
        nd.setSiguiente(null);
    }
    public void limpiar(){
        while(cabeza != null){
            eliminar(cabeza);
        }
    }
    public void agregarJugador(jugador nuevo){
        if(cabeza == null){
            setCabeza(nuevo);
        }else{
            ultimo().setSiguiente(nuevo);
        }
    }
    public int lanzar (){
        int resultado,dado;
        dado = (int) Math.floor(Math.random()*6+1);
        resultado = dado*100;
        return resultado;
    }
    public void iniciar(pista pi){
        int pos = 0;
        while(pos<3){
            jugador temp = cabeza;
            while(temp != null){   
                if(temp.getRecorrido() < pi.getDistancia()){
                temp.setRecorrido(temp.getRecorrido()+lanzar());
                JOptionPane.showMessageDialog(null, "El jugador: "+
                        temp.getNombre()+
                        " Avanza : "+lanzar()+
                        " metros ");
                        if(temp.getRecorrido() >= pi.getDistancia()){
                            pos++;
                            JOptionPane.showMessageDialog(null, "El jugador "+temp.getNombre()+" ha cruzado la meta " + pos);
                            if(pos == 1){pi.setPuesto1("Puesto 1: " + temp.getNombre());}
                            if(pos == 2){pi.setPuesto2("Puesto 2: " + temp.getNombre());}
                            if(pos == 3){pi.setPuesto3("Puesto 3: " +temp.getNombre());}
                        }
                        temp = temp.getSiguiente(); 
                }else{
                    temp = temp.getSiguiente();
                }
            }
        }
    }
}
