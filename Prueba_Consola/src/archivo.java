/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.StringTokenizer;
/**
 *
 * @author Windows
 */
public class archivo {
    private String rutaArchivo;
    File archivo;
    
    public void Archivo()
    {
        rutaArchivo = "C:\\Projects\\log.txt";
    }
    public void setRutaArchivo(String nom)
    {
        rutaArchivo = nom;
    }
    
    public String getRutaArchivo()
    {
        return rutaArchivo;
    }
    public void guardar(pista p) throws IOException
    {
        archivo = new File("C:\\Projects\\log.txt");
        BufferedWriter escribir; 
        if (archivo.exists())
        {
            escribir = new BufferedWriter(new FileWriter(archivo, true));
            escribir.write(p.getPuesto1()+"\t");
            escribir.write(p.getPuesto2()+"\t");
            escribir.write(p.getPuesto3()+"\t");            
            escribir.write(p.getIdp()+"\t");
            escribir.newLine();
        }else{
               escribir = new BufferedWriter(new FileWriter(archivo));
               escribir.write(p.getPuesto1()+"\t");
               escribir.write(p.getPuesto2()+"\t");
               escribir.write(p.getPuesto3()+"\t");            
               escribir.write(p.getIdp()+"\t");
               escribir.newLine();
        }
        escribir.close();
    }
    public String leer(String rutaArchivo) throws FileNotFoundException, IOException
    {
        String linea;
        String verDatosArchivo = "";
        pista p;
        FileReader fichero = new FileReader(rutaArchivo);
        BufferedReader leer = new BufferedReader(fichero);
        while ((linea = leer.readLine()) != null)
        {
            StringTokenizer campo = new StringTokenizer(linea,"\t"); 
            String p1 = campo.nextToken().trim();
            String p2 = campo.nextToken().trim();
            String p3 = campo.nextToken().trim();
            String npt = campo.nextToken().trim();
            p = new pista();
            p.setPuesto1(p1);
            p.setPuesto2(p2);
            p.setPuesto3(p3);
            p.setIdp(npt);
            verDatosArchivo = verDatosArchivo+String.valueOf(linea+"\n");
        }
        leer.close();
        return verDatosArchivo;
    }
    
}
