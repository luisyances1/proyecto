/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows
 */
public class jugador {

    String color;
    String nombre;
    jugador siguiente;
    int recorrido;

    jugador() {
        color = "";
        nombre = "";
        siguiente = null;
        recorrido = 0;
    }

    public void setNombre(String nom) {
        nombre = nom;
    }

    public void setColor(String cl) {
        color = cl;
    }

    public void setSiguiente(jugador sig) {
        siguiente = sig;
    }

    public void setRecorrido(int rec) {
        recorrido = rec;
    }
    
    

    public String getColor() {
        return color;
    }

    public String getNombre() {
        return nombre;
    }

    public jugador getSiguiente() {
        return siguiente;
    }

    public int getRecorrido() {
        return recorrido;
    }
    
}
